# legolize

LEGOlize your images - budget [Mosaic Maker](https://petapixel.com/2017/03/02/lego-photo-booth-helps-build-portrait-bricks/) with [Pillow](https://python-pillow.org/).

## Example

![example](example.png)
